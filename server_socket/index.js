const WebSocket = require('ws');

const wss = new WebSocket.Server({
    port: 3030,
    perMessageDeflate: {
        zlibDeflateOptions: {

            chunkSize: 1024,
            memLevel: 7,
            level: 3
        },
        zlibInflateOptions: {
            chunkSize: 10 * 1024
        },

        clientNoContextTakeover: true,
        serverNoContextTakeover: true,
        serverMaxWindowBits: 10,
        concurrencyLimit: 10,
        threshold: 1024
    }

});

var clients = [];


wss.on('connection', (client) => {

    client.send(Date.now());


    client.on('close', () => {

    });

    client.on('message', (msg) => {
        console.log(msg);
    })
});





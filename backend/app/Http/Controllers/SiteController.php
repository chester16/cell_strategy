<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    /**
     * @return Factory|\Illuminate\View\View
     */
    public function index(){
        $token = Auth::user()->token;
        $name = Auth::user()->name;

        return view('index')->with([
            'token' => $token,
            'name' => $name
        ]);
    }
}

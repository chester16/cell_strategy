
export default class WebSocketConnection {

    constructor(host) {
        const self = this;
        this.ws = new WebSocket('ws://' + host);

        this.ws.onopen = () => {
            console.log('connected');
            self.ws.send('Hello!');
        };

        this.ws.onclose = (event) => {
            console.log('closed');
        };

        this.ws.onmessage = (event) => {
            console.log('Data: ' + event.data);
        };
    }

}

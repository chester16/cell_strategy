import React from 'react';
import {connect} from 'react-redux';

class Header extends React.Component{

    logout(){
        window.axios.post('/logout')
            .finally(() => {
                window.location.replace('/');
            });
    }

    btnClick(){
        this.logout();
    }

    render() {
        return (
            <div className="header">
                <div className="item logged-as">Вы вошли как {this.props.username}</div>
                <div className="item">
                    <button className="btn-small btn-default" onClick={this.btnClick.bind(this)}>Выйти</button>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        store: state
    }),
    dispatch => ({
        logout: (newToken) => {
            dispatch({type: 'logout'});
        }
    })
)(Header);

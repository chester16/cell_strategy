import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {Provider} from 'react-redux';
import Header from './Header';
import {store} from './../store/index';


export default class Main extends Component {

    constructor(props) {
        super(props);
        this.token = window.$('#app').attr('data-token');
        this.username = window.$('#app').attr('data-username');
        store.dispatch({type: 'ws_connect', token: this.token});
        store.dispatch({type: 'set_username', username: this.username});
    }


    render() {
        return (
            <Provider store={store}>
                <Header username={this.username}></Header>
                <div className="container">
                </div>
            </Provider>

        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Main />, document.getElementById('app'));
}

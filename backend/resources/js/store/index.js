import {createStore} from "redux";

var reducer = (state = {}, action) => {
    if(action.type === 'ws_connect'){
        state.token = action.token;
    }
    if(action.type === 'set_username'){
        state.username = action.username;
    }

    return state;
};

export const store = createStore(reducer);

store.subscribe(() => {

});


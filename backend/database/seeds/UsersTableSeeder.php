<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            'name' => 'Rasskazov_P',
            'email' => 'rasskazov@mail.local',
            'password' => bcrypt('9340126abc'),
            'token' => \Illuminate\Support\Str::random(64)
        ));
    }
}
